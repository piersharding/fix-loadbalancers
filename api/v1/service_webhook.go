/*

Copyright 2022 Piers Harding
Copyright 2022 SKA Observatory

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
// +kubebuilder:docs-gen:collapse=Apache License

// Go imports
package v1

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"sigs.k8s.io/controller-runtime/pkg/webhook/admission"

	// Import all Kubernetes client auth plugins (e.g. Azure, GCP, OIDC, etc.)
	// to ensure that exec-entrypoint and run can make use of them.

	_ "k8s.io/client-go/plugin/pkg/client/auth"
	"sigs.k8s.io/controller-runtime/pkg/client"

	//+kubebuilder:scaffold:imports

	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/serializer"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
)

var (
	codecs       = serializer.NewCodecFactory(runtime.NewScheme())
	deserializer = codecs.UniversalDeserializer()
)

type ServiceAnnotator struct {
	Client  client.Client
	decoder *admission.Decoder
}

// +kubebuilder:webhook:path=/mutate-v1-loadbalancer,mutating=true,failurePolicy=ignore,groups="",resources=services,verbs=create;update;delete,versions=v1,admissionReviewVersions=v1,sideEffects=None,name=msvc.skao.int

// https://github.com/trstringer/kubernetes-mutating-webhook/blob/main/cmd/root.go
func (a *ServiceAnnotator) Handle(ctx context.Context, req admission.Request) admission.Response {
	log := logf.FromContext(ctx)

	// log.Info("Annotated Service request: " + fmt.Sprintf("%v", req))

	// Decode the Service from the AdmissionReview.
	svc := corev1.Service{}
	if _, _, err := deserializer.Decode(req.Object.Raw, nil, &svc); err != nil {
		return admission.Errored(http.StatusBadRequest, err)
	}

	// mutate the fields in Service
	if svc.Annotations == nil {
		svc.Annotations = map[string]string{}
	}
	log.Info("Annotated Service: " + fmt.Sprintf("operation: %s %s/%s", req.Operation, svc.Namespace, svc.Name))
	if req.Operation == "DELETE" {
		log.Info("Service: (RETURNING) " + fmt.Sprintf("operation: %s %s/%s", req.Operation, svc.Namespace, svc.Name))
		return admission.Allowed("Delete Service allowed")
	}

	if _, ok := svc.Annotations["skip-fix-loadbalancers"]; ok {
		log.Info("SVC: (RETURNING) " + fmt.Sprintf("annotations: %v %s/%s", svc.Annotations, svc.Namespace, svc.Name))
		return admission.Allowed("Skip SVC fix")
	}

	if svc.Spec.Type == "LoadBalancer" {
		log.Info("Incoming Service: " + fmt.Sprintf("%v", svc.Spec))
		svc.Annotations["fix-loadbalancers-mutating-admission-webhook"] = fmt.Sprintf("was: %s", svc.Spec.Type)
		svc.Spec.Type = "ClusterIP"
		svc.Spec.ExternalTrafficPolicy = ""
		svc.Spec.AllocateLoadBalancerNodePorts = nil
	}

	log.Info("Service: (PATCHED: remove LoadBalancer) " + fmt.Sprintf("operation: %s %s/%s", req.Operation, svc.Namespace, svc.Name))

	marshaledService, err := json.Marshal(svc)
	if err != nil {
		return admission.Errored(http.StatusInternalServerError, err)
	}

	// admission.Response
	return admission.PatchResponseFromRaw(req.Object.Raw, marshaledService)
}

func (a *ServiceAnnotator) InjectDecoder(d *admission.Decoder) error {
	a.decoder = d
	return nil
}
